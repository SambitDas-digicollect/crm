//
//  ForgotPasswordSecondVC.swift
//  CRM
//
//  Created by Sambit Das on 25/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ForgotPasswordSecondVC: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var deepBlueView: UIView!
    @IBOutlet weak var nextButton: UIView!
    @IBOutlet weak var arghhLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
         
        self.backgroundView.backgroundColor = UIColor(patternImage: UIImage(named: "Path5320")!)
        nextButton.layer.cornerRadius = 10
        deepBlueView.layer.cornerRadius = 10
        arghhLabel.layer.cornerRadius = 30
        arghhLabel.layer .masksToBounds = true
    }
    

}
