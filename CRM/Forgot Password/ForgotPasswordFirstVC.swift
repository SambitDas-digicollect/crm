//
//  ViewController.swift
//  CRM
//
//  Created by Sambit Das on 25/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var BackgroundView: UIView!
    @IBOutlet weak var deepBlueView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var arrghLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BackgroundView.backgroundColor = UIColor(patternImage: UIImage(named: "Path5320")!)
        deepBlueView.layer.cornerRadius = 10
        nextButton.layer.cornerRadius = 10
        arrghLabel.layer.cornerRadius = 30
        arrghLabel.layer.masksToBounds = true
        
        
    }


}

