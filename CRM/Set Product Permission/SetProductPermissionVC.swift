//
//  SetProductPermissionVC.swift
//  CRM
//
//  Created by Sambit Das on 27/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class SetProductPermissionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var titleArray = ["Accounting","CRM","Forms","Contract Management","Cloud","Chat","Expesnse Management","SCM","IMS","HRMS","DigiPOS","DigiPay","Helpdesk","DigiPlay","MarketPlace"]
    var secondTitle = [String]()
    var sourceIndex = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inviteButton.layer.cornerRadius = 10
        cancelButton.layer.cornerRadius = 10
        
      
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        for (index,each) in self.sourceIndex.enumerated() {
            if each == indexPath {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! SetProductSecondCell
                cell.secondDelegate = self
                cell.indexInfo = indexPath
                cell.NameLabel.text = self.secondTitle[index]
                cell.switch2Button.isOn = true
                return cell
            }
        }
        

            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! SetProductFirstTCell
            cell.name.text = titleArray[indexPath.row]
            cell.indexValue = indexPath
            cell.delegateObj = self
            cell.switchButton.isOn = false
            return cell
    }
}

extension SetProductPermissionVC: cellDelegate, SecondCellDelegate {
    func onClickSiwtch(name: String, indexInfo: IndexPath) {
        if self.sourceIndex.contains(indexInfo) {
            if let index = self.sourceIndex.firstIndex(of: indexInfo) {
                self.secondTitle.remove(at: index)
                self.sourceIndex.remove(at: index)
            }
        } else {
            self.secondTitle.append(name)
            self.sourceIndex.append(indexInfo)
            self.tableView.reloadData()
        }
        self.tableView.reloadData()
    }
    func onClickSecondSwitch(indexValue: IndexPath) {
        print("executed !!")
        if self.sourceIndex.contains(indexValue) {
            if let index = self.sourceIndex.firstIndex(of: indexValue) {
                self.secondTitle.remove(at: index)
                self.sourceIndex.remove(at: index)
                self.tableView.reloadData()
            }
        }
    }
}
