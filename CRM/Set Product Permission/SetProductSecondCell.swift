//
//  SetProductSecondCell.swift
//  CRM
//
//  Created by Sambit Das on 27/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

protocol SecondCellDelegate: AnyObject {
    func onClickSecondSwitch(indexValue: IndexPath)
}
class SetProductSecondCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var switch2Button: UISwitch!
    @IBOutlet weak var readRadioButton: UIButton!
    @IBOutlet weak var writeRadioButton: UIButton!
    @IBOutlet weak var adminRadioButton: UIButton!
    var indexInfo: IndexPath?
    weak var secondDelegate: SecondCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchPressed(_ sender: Any) {
        print("buttonpressed")
        guard let index = self.indexInfo else { return }
        self.secondDelegate.onClickSecondSwitch(indexValue: index)
    }
    @IBAction func readRadioButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
        sender.isSelected = false
            readRadioButton.isSelected = false
            writeRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            
        }else{
            sender.isSelected = true
            readRadioButton.isSelected = true
            writeRadioButton.isSelected = false
            adminRadioButton.isSelected = false
        }
    }
    @IBAction func writeRadioButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
        sender.isSelected = false
            readRadioButton.isSelected = false
            writeRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            
        }else{
            sender.isSelected = true
            readRadioButton.isSelected = false
            writeRadioButton.isSelected = true
            adminRadioButton.isSelected = false
        }
    }
    @IBAction func adminRadionButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
        sender.isSelected = false
            readRadioButton.isSelected = false
            writeRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            
        }else{
            sender.isSelected = true
            readRadioButton.isSelected = false
            writeRadioButton.isSelected = false
            adminRadioButton.isSelected = true
        }
    }
}
