//
//  SetProductFirstTCell.swift
//  CRM
//
//  Created by Sambit Das on 27/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
protocol cellDelegate: AnyObject {
    func onClickSiwtch(name: String, indexInfo: IndexPath)
}
class SetProductFirstTCell: UITableViewCell {
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    weak var delegateObj: cellDelegate!
    var indexValue: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func switchPressed(_ sender: Any) {
        guard let index = self.indexValue else { return }
        self.delegateObj.onClickSiwtch(name: (self.name.text)!, indexInfo: index)
    }

}
