//
//  InviteAnEmployeeVC.swift
//  CRM
//
//  Created by Sambit Das on 27/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class InviteAnEmployeeVC: UIViewController {

    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var otherRadioButton: UIButton!
    @IBOutlet weak var userRadioButton: UIButton!
    @IBOutlet weak var adminRadioButton: UIButton!
    @IBOutlet weak var superAdminRadioButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var dateTextfield: underLineTextfield!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelButton.layer.cornerRadius = 10
        nextButton.layer.cornerRadius = 10
        
          //rightView for date textfield
                dateTextfield.rightView = UIImageView(image: UIImage(systemName: "calendar"))
                dateTextfield.rightView?.tintColor = .white
                dateTextfield.rightView?.frame = CGRect(x: 0, y: 5, width: 20 , height:20)
                dateTextfield.rightViewMode = .always

    }
    @IBAction func maleButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
                   sender.isSelected = false
            maleRadioButton.isSelected = false
            femaleRadioButton.isSelected = false
            otherRadioButton.isSelected = false
               }else{
                  sender.isSelected = true
            maleRadioButton.isSelected = true
            femaleRadioButton.isSelected = false
            otherRadioButton.isSelected = false
            }
    }
    
    @IBAction func femaleButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
               sender.isSelected = false
               maleRadioButton.isSelected = false
               femaleRadioButton.isSelected = false
               otherRadioButton.isSelected = false
           }else{
              sender.isSelected = true
              maleRadioButton.isSelected = false
              femaleRadioButton.isSelected = true
              otherRadioButton.isSelected = false
        }
        
    }
    
    @IBAction func otherButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
               sender.isSelected = false
               maleRadioButton.isSelected = false
               femaleRadioButton.isSelected = false
               otherRadioButton.isSelected = false
           }else{
              sender.isSelected = true
              maleRadioButton.isSelected = false
              femaleRadioButton.isSelected = false
              otherRadioButton.isSelected = true
        }
    }
    @IBAction func userButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            userRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            superAdminRadioButton.isSelected = false
        }else{
            sender.isSelected = true
            userRadioButton.isSelected = true
            adminRadioButton.isSelected = false
            superAdminRadioButton.isSelected = false
        }
    }
    
    @IBAction func adminButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            userRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            superAdminRadioButton.isSelected = false
        }else{
            sender.isSelected = true
            userRadioButton.isSelected = false
            adminRadioButton.isSelected = true
            superAdminRadioButton.isSelected = false
        }
    }
    @IBAction func superAdminButtonPressed(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            userRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            superAdminRadioButton.isSelected = false
        }else{
            sender.isSelected = true
            userRadioButton.isSelected = false
            adminRadioButton.isSelected = false
            superAdminRadioButton.isSelected = true
        }
    }
}
class underLineStackView : UIStackView{
    
override func  awakeFromNib() {
    super.awakeFromNib()
    
    let btmBorder = UIView(frame: .zero)
                      btmBorder.backgroundColor = .white
                      self.addSubview(btmBorder)
               btmBorder.translatesAutoresizingMaskIntoConstraints = false
                      btmBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
                      btmBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
                      btmBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
                      btmBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 2).isActive = true

              self.layer.masksToBounds = true
    
    }
    
}

class underLineTextfield : UITextField{
        
    override func  awakeFromNib() {
        super.awakeFromNib()
        
        self.borderStyle = .none
       
        let btmBorder = UIView(frame: .zero)
                          btmBorder.backgroundColor = .white
                          self.addSubview(btmBorder)
                   btmBorder.translatesAutoresizingMaskIntoConstraints = false
                          btmBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
                          btmBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
                          btmBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
                          btmBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.layer.masksToBounds = true
        
    }

}
